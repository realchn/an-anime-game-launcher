# Start Fenster
splash:
  title: Starte Launcher
  phrases:
    - Bruder, lass uns {game} spielen...
    - Paimon am ärgern...
    - Versuche Yae zu ziehen...
    - Materialien farmen...
    - Abyss bewältigen...
    - Errungenschaften sammeln...
    - Phys Qiqi erstellen...
    - Opfer für 5* bringen...
    - Freunde für Koop finden...

# Launcher window
launcher:
  # Progress bar
  progress:
    pause: Pausieren
    resume: Fortsetzen
    
    # Game installation
    game:
      downloading: Spiel ist am herunterladen...
      unpacking: Spiel wird entpackt...
      applying_changes: Änderungen werden angewandt...
      deleting_outdated: Lösche veraltete dateien...
      integrity_check: Verifiziere Dateien...
      download_mismatch_files: Lade nicht übereinstimmenden Dateien herunter...
    
    # Voice packages installation
    voice:
      deleting: Lösche Sprachpakete...
      downloading: Sprachpaket {voice} wird heruntergeladen...
      unpacking: Sprachpaket {voice} wird entpackt..

  # Launcher states
  states:
    # When the game should be installed or updated
    installation:
      install_wine: Wine installieren
      install_dxvk: DXVK installieren
      install: Installieren
      update: Updaten
      
      apply_changes:
        title: Änderungen anwenden
        hint: hdiff-Änderungen an den Spieldateien anwenden

      remove_outdated:
        title: Alte Dateien löschen 
        hint: Veraltete Dateien löschen 

    # When the game should be patched
    patching:
      # Patch unavailable
      unavailable:
        title: Patch nicht verfügbar
        hint: Diese Spielversion hat kein patch zu verfügung.
              Bitte warte ein paar Tage um zusehen ob ein test oder stabil Patch veröffentlicht worde.
      
      # Patch is in testing
      test:
        title: Test Patch anwenden
        hint: Diese Spielversion hat ein Test Patch verfügbar.
              Sie können ein paar Tage warten um den Stabilen patch zu installieren oder sie können den Test Patch installieren und ihr Account riskieren.

      # Patch is stable
      stable: Patch anwenden

    # When the game is ready for playing
    ready:
      launch: Starten
      predownload: Update vorinstallieren

# Einstellungs Fenster
settings:
  # General
  general:
    title: General
    items:
      # Sprach-Auswahl
      lang:
        # Launcher sprache
        launcher:
          title: Launcher
          items:
            en-us: English (US)
            ru-ru: Русский
            es-es: Español
            de-de: Deutsch
            fr-fr: Français
            it-it: Italiano
            ja-jp: 日本語
            hu-hu: Magyar
            id-id: Bahasa Indonesia
            nb-no: Norsk
            zh-cn: 简体中文
            uwu: Engwish

        # Spiel sprachpaket auswahl
        voice:
          title: Sprachpaket
          tooltip: Bitte wähle im Spiel das Sprachpaket aus um es zu benutzen
          items:
            en-us: Englisch (US)
            ja-jp: Japanisch
            ko-kr: Koreanische
            zh-cn: Chinesisch

      # Launcher thema
      theme:
        title: Thema
        items:
          system: System
          light: Hell
          dark: Dunkel
      
      # Discord RPC
      discord:
        title: Discord RPC
        settings:
          title: Discord RPC Einstellung
          items:
            timer: Zeige geschätzte Zeit
            in-launcher: Launcher text
            in-game: im Spiel text
            selectIcon: Icon auswählen

      # Some buttons
      buttons:
        winetricks: winetricks
        winecfg: winecfg
        launcher: Launcher Ordner öffnen
        game: Spiel Ordner öffnen
        repair_game: spiel-dateien reparieren
        clear_cache: cache leeren

      # Patch-related settings
      patch:
        title: Patch
        items:
          patch_version: 'Patch version:'
          updating_info: 'Aktualisiere Patch-Informationen...'
          buttons:
            revert_patch: Patch rückgängig machen
            apply_patch: Patch anwenden
            reapply_patch: Patch erneut anwenden

  # Verberssungen
  enhancements:
    title: Verbesserungen

    # Enhancements related to the wine
    wine:
      title: Wine
      items:
        # HUD
        hud:
          title: HUD
          items:
            none: Aus
            dxvk: DXVK
            mangohud: MangoHUD

        # Wine synchronization
        winesync:
          title: Wine Synchronisation
          tooltip: Esync ist ein Mechanismus zur Synchronisierung von Multi-Thread-Operationen womit die Leistung Ihres Spiels verbessert wird.
                  Fsync ist eine verbesserte Version von Esync, die auf bestimmten Kernel-Versionen funktioniert
          items:
            none: Aus
            esync: Esync
            fsync: Fsync
            futex2: Futex2

        # AMD FSR
        fsr:
          title: AMD FSR
          tooltip: Diese Option aktiviert AMD FidelityFX Super Resolution (FSR),
                  womit kleine Resolution hochskaliert wird um FPS-verlust zu vermeiden.

        # Wine Virtual Desktop
        winevd:
          title: Virtual Desktop
          settings:
            title: Virtual Desktop einstellung
            items:
              width: Breite
              height: Höhe

    # Enhancements related to the game
    game:
      title: Spiel
      items:
        # GameMode
        gamemode:
          title: Benutze GameMode
          tooltip:
            enabled: GameMode ist ein Program welches die Leistung verbessert
            disabled: ⚠️ Sie haben GameMode nicht installiert

        # Borderless Window
        borderless_window:
          title: Als randloses Fenster starten
          tooltip: Entfernt die Fensterränder beim Spielen im Fenstermodus.
                   Um in einem randlosen Fenster im Vollbildmodus zu spielen, drücken Sie die Tastenkombination alt+enter.

        # Unlock FPS
        fps_unlocker:
          title: Entsperre FPS
          tooltip: Diese option entsperrt/entfernt die 60 FPS limiterung

        # Use separate terminal window to run the game
        use_terminal:
          title: Terminal aktivieren
          tooltip: Wenn diese Option aktiviert ist, führt der Launcher den Wine Befehl in einem separaten Terminalfenster aus

    # Enhancements related to the launcher
    launcher:
      title: Launcher
      items:
        # Delete logs
        purge_logs:
          # Game logs (DXVK)
          game:
            title: DXVK logs löschen
            tooltip: When diese option angeschaltet ist löscht
                    der Launcher automatisch alle DXVK logs

          # Launcher logs
          launcher:
            title: Launcher logs löschen
            tooltip: Launcher logs werden nach diese angegebenen Zeit gelöscht
            items:
              1d: 1 Tag
              3d: 3 Tage
              5d: 5 Tage
              7d: 1 Woche
              14d: 2 Wochen
              never: Niemals

  # Runners
  runners:
    title: Wine version
    items:
      recommended:
        title: Nur empfohlene anzeigen
        tooltip: Falls diese option eingeschaltet ist werden nur Wine versionen angezeigt
                 die funktioneren.

  # DXVKs
  dxvks:
    title: DXVK
    items:
      recommended:
        title: Nur empfohlene anzeigen
        tooltip: Falls diese option eingeschaltet ist werden alte DXVK versionen nicht angezeigt

  # Shaders
  shaders:
    title: Shaders
    items:
      shaders:
        title: Shaders
        tooltip: Benutze die Home/POS1 taste um die Shader in-spiel zu togglen
        items:
          none: Aus
          custom: Custom

      author: 'Autor: {author}'
      no_images: Keine Bilder verfügbar
      not_installed: Sie haben vkBasalt und Reshade-Shader nicht installiert

  # Environmantal variables manager
  environment:
    title: Umgebungsvariablen
    items:
      # Table rows
      table:
        name: Name
        value: Wert
      
      # Table buttons
      buttons:
        add: Hinzufügen
        delete: Löschen

# Notifications
notifications:
  # Launcher update
  launcher_update_available:
    title: 'Launcher update verfügbar: {from} -> {to}'
    body: Sie können das update von {repository} herunterladen

  # Before telemetry check when iputils is not downloaded
  iputils_package_required:
    title: An Anime Game Launcher
    body: Sie müssen iputils für die Telemetrieüberprüfung installiert haben

  # When telemetry servers are not disabled
  telemetry_not_disabled:
    title: An Anime Game Launcher
    body: Telemetry server sind nicht deaktiviert.

  # Before patch applying when xdelta3 package is not downloaded
  xdelta3_package_required:
    title: An Anime Game Launcher
    body: Sie müssen xdelta3/xdelta installiert haben um den Patch anzuwenden

  # If patch wasn't applied because of some error
  patch_applying_error:
    title: An Anime Game Launcher
    body: Der Patch wurde nicht erfolgreich angewendet. Bitte überprüfen Sie Ihre Log-Datei, um den Grund dafür zu finden, oder fragen Sie jemanden auf unserem Discord-Server

  # Patch repositories are not available
  patch_repos_unavailable:
    title: An Anime Game Launcher
    body: Alle Patch-Repositories sind nicht verfügbar. Sie können das Spiel starten aber der Launcher kann sich nicht sicher sein dass es gepatcht ist.

  # HDiffPatch couldn't successfully apply game files changes
  game_changes_applying_error:
    title: Ein Fehler geschah, während das Spiel aktualisiert wurde
    body: '{files} Dateien konnten nicht aktualisiert werden'

# ToS violation warning window
tos_violation:
  title: ToS-Verletzungswarnung
  heading: ⚠️ Sei gewarnt   
  body: Dieser Launcher ist ein inoffizielles Tool, das in keiner Weise mit {company} oder {company_alterego} in Verbindung steht.
        Dieses Tool wurde entwickelt, um das Spielen von {game} unter Linux zu erleichtern. Es wurde mit dem einzigen Ziel entwickelt, das Spiel mit weniger Aufwand zu installieren und zu starten.
        Dies geschieht durch die Verwendung vorhandener Komponenten und vereinfacht die Erfahrung für den Benutzer.
        Einige der hier verwendeten Komponenten verstoßen jedoch wahrscheinlich gegen die {company}-Nutzungsbedingungen für {game}.
        Wenn Sie diesen Launcher verwenden, könnte ihr Spielerkonto von {company}/{company_alterego} als nicht TOS-konform identifiziert werden.
        Wenn dies geschieht, da Ihr Konto gegen die TOS verstößt kann {company}/{company_alterego} entscheiden, was Sie tun wollen Inklusive Sperre.
        Wenn Sie des Risikos bewusst sind, dass Sie versuchen das Spiel inoffiziell zu spielen, drücke "Ich verstehe das Risiko" und lass uns die Welt von Teyvat erforschen!
  buttons:
    ok:
      title: Ich verstehe das Risiko
      tooltip: Sie sollten diesen Text wirklich lesen, Er ist sehr wichtig.
    cancel: Abbrechen
    discord: Unser Discord Server

# Analytics window
analytics:
  title: Yanfeis Kommission...
  header: Teilnahme an der anonymen Datenerhebung

  body:
    - Um die aktive Nutzerbasis für Linux zu zählen, möchte Yanfei bei jedem Update des Spiels Ihre IP-Adresse erfassen
    - Die IP-Adresse wird zu Sicherheitszwecken gehasht

  actions:
    share_country:
      title: Land teilen
      hint: Erlauben Sie Yanfei das Land zu speichern das Ihre IP-Adresse anzeigt, um die Statistiken detaillierter zu gestalten.
            Es werden keine anderen Daten als das Land gespeichert

    participate: Teilnehmen
    skip: Überspringen
    skip_forever: Überspringen und nicht mehr nach fragen

# Screenshots window
screenshots:
  heading: Schnappschüsse
  info: Klicken Sie auf ein Schnappschuss, um es zu öffnen.
  buttons:
    more: Mehr laden
    folder: Öffne Schnappschüsse
  no_images: Keine Schnappschüsee verfügbar